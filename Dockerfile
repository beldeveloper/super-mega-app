FROM alpine:latest
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY app ./
EXPOSE 7005
EXPOSE 7006
EXPOSE 7007
CMD ["./app"]
