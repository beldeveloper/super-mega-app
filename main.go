package main

import (
	"log"
	"net/http"
	"os"
)

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		_, err := w.Write([]byte("Master branch"))
		if err != nil {
			log.Printf("write http response: %v\n", err)
		}
	})
	port := os.Getenv("SMA_PORT")
	err := http.ListenAndServe(":"+port, nil)
	if err != nil {
		log.Fatalf("listen http on port %s: %v\n", port, err)
	}
}
